;;; practice-mode -- A major mode to edit Lauterbach Practice files

;;; Commentary:


;;; Code:

(defconst practice-mode-font-lock-keywords-1
  `(;; Flow control keywords
    (,(regexp-opt '("stop" "end" "continue" "do" "enddo" "run" "gosub" "return" "goto" "jumpto")
                  'words)
     . font-lock-keyword-face)
    ;; Conditionals control keywords
    (,(regexp-opt '("if" "else" "while" "repeat" "on" "globalon" "wait")
                  'words)
     . font-lock-keyword-face)
    )
  "Keyword regexps for pratice mode.")


(defconst practice-mode-font-lock-keywords-2
  `(;; Macro visibility types
    (,(regexp-opt '("private" "local" "global" "entry") 'words) . font-lock-type-face)
    ;; Macro names
    ("\\&\\&?\\w+" . font-lock-variable-name-face)
    )
  "Macro related regexps for pratice mode.")


;; INPUT OUTPUT
;; print beep enter inkey sreen.\.+

;; FIle Operations
;; open close read write append
;; file(?) handle: #[0-9]


(defvar practice-indent 4)

(defvar practice-mode-font-lock-keywords)
(setq practice-mode-font-lock-keywords
      (append practice-mode-font-lock-keywords-1
              practice-mode-font-lock-keywords-2
              '(;; Label
                ("^\\w+:" . font-lock-constant-face)
                ;; Line continuation
                ("\\\\$" . font-lock-warning-face))
        ))


(define-derived-mode practice-mode fundamental-mode "Practice"
  (make-local-variable 'indent-line-function)
  (setq indent-line-function 'practice-indent-line)
  (setq font-lock-defaults
        '((practice-mode-font-lock-keywords)
          nil
          t
          ((?_ . "w")
           (?\( . "()")
           (?\) . ")(")
           (?\; . "<") ; Lisp style comment
           (?/ . ". 12") ; C style comment
           (?\n . ">") ; Comment end
           ))))


(add-to-list 'auto-mode-alist '("\\.cmm\\'" . practice-mode))



(defun find-indentation ()
  "."
  (cond
   ;; First line indents to zero
   ((eq (line-number-at-pos) 1) 0)
   ;; Indent multiline statements
   ((inside-multiline) (+ (get-current-indent) practice-indent))
   ;; Labels always indent to zero
   ((practice-line-is-label) 0)
   ;; End of a block, indent to block start
   ((current-is-block-end) (get-block-start-indent))
   ;; Right after block start, indent one step more
   ((prev-is-block-start) (+ (get-prev-indent) practice-indent))
   ;; Indent to the last statements if nothing else matches
   (t (get-prev-indent))
   ))


(defun practice-indent-line ()
  "."
  (let ((old-point (point))
        (old-indent (current-indentation))
        (new-indent (find-indentation)))
    (indent-line-to new-indent)
    ;; Move point with indentation.
    (let ((new-point (+ old-point (- new-indent old-indent)))
          (indent-pos (+ (line-beginning-position) new-indent))
          (eol-pos (line-end-position)))
      (cond ((< new-point indent-pos)
             (goto-char indent-pos))
            ((> new-point eol-pos)
             (goto-char eol-pos))
            (t (goto-char new-point)))
      )))





(defun practice-line-is-label ()
  ".."
  (let ((old-point (point)))
    (practice-goto-statement-start)
    (let ((result
           (re-search-forward "[ ]*\\w+:\\s-*$" (line-end-position) t)))
      (goto-char old-point)
      result
      )))


(defun inside-multiline ()
  ".."
  (/= (line-beginning-position) (find-start)))


(defun prev-is-block-start ()
  ".."

  (let ((old-point (point)))
    (practice-goto-previous-statement)
    (let ((result
           (re-search-forward "^\\s-*(\\s-*$" (line-end-position) t)))
      (goto-char old-point)
      result
      )))


(defun current-is-block-end ()
  ".."
  (let ((old-point (point)))
    (practice-goto-statement-start)
    (let ((result
           (if (re-search-forward "^\\s-*)\\s-*$" (line-end-position) t)
               t
             nil)))
      (goto-char old-point)
      result
      )
    )
  )


(defun get-current-indent ()
  ".."
  (let ((old-point (point)))
    (practice-goto-statement-start)
    (let ((indent (current-indentation)))
      (goto-char old-point)
      indent)))


(defun get-prev-indent ()
  ".."
  (let ((old-point (point)))
    (practice-goto-previous-statement)
    (let ((indent (current-indentation)))
      (goto-char old-point)
      indent)))


(defun get-block-start-indent ()
  ".."
  (let ((old-point (point)))
    (practice-goto-block-start)
    (let ((indent (current-indentation)))
      (goto-char old-point)
      indent)))



(defun find-start (&optional l)
  "Find start position of the statement/line where point is located.
L: how many lines to go back"
  (let ((prev (or l 0)))
    (if (eq ?\\ (char-before (line-end-position prev)))
        (find-start (1- prev))
      (line-beginning-position (1+ prev))
      ))
  )


(defun practice-goto-statement-start ()
  ".."
  (goto-char (find-start)))


(defun practice-goto-previous-statement ()
  ".."
  ;; Just for my future self: progn returns the last value in the
  ;; sequence. This emulates something like C's do-while.
  (while (progn
           ;; Go to start of current statement and then one character back.
           ;; We should now be at the end of the previous line in the buffer.
           (goto-char (1- (find-start)))
           ;; This is the loop condition: repeat until at start of
           ;; buffer or while line is empty
           (and (/= (point) 1)
               (re-search-backward "^\\s-*$" (line-beginning-position) t))))
  (practice-goto-statement-start))


(defun practice-goto-block-start ()
  ".."
  (while (and (/= (point) 1)
              (not (prev-is-block-start)))
    (practice-goto-previous-statement)
    ;; Recursion if a block end has been found inside the block
    (when (current-is-block-end)
      (practice-goto-block-start))
    )
  (practice-goto-previous-statement)
  )


(provide 'practice-mode)
;;; practice-mode.el ends here
